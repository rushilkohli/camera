package camera;

/**
 *
 * @author rushil
 */
public abstract class DigitalCamera {
    private String make;
    private String model;
    private double megapixels;
    private double internalMemorySize;
    private double externalMemorySize;

    public DigitalCamera(String make, String model, double megapixels, double internalMemorySize, double externalMemorySize) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
        this.internalMemorySize = internalMemorySize;
        this.externalMemorySize = externalMemorySize;
    }
    
    public abstract String describeCamera();
}
