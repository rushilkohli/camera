package camera;

/**
 *
 * @author rushil
 */
public class PhoneCamera extends DigitalCamera{
    
    private String make;
    private String model;
    private double megapixels;
    private double internalMemorySize;
    private double externalMemorySize;
    public PhoneCamera(String make, String model, double megapixels, double internalMemorySize, 
                        double externalMemorySize) {
        super(make, model, megapixels, internalMemorySize, externalMemorySize);
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
        this.internalMemorySize = internalMemorySize;
        this.externalMemorySize = externalMemorySize;
    }

    @Override
    public String describeCamera() {
        return ("Make: " + make + "\n" + "Model: " + model + "\n" + "Megapixels: " + 
                megapixels +"px" + "\n" + "Internal Memory Size: " + internalMemorySize 
                + "GB" + "\n" + "External Memory Size: " + externalMemorySize + "GB" ); 
    }
    
    
}
