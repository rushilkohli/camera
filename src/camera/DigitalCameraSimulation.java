package camera;

/**
 *
 * @author rushil
 */
public class DigitalCameraSimulation {
    public static void main(String[] args) {
        PointAndShootCamera pointShoot = new PointAndShootCamera("Canon", "PowerShot A590", 8.0, 0, 16);
        PhoneCamera phone = new PhoneCamera("Apple", "iPhone", 6.0, 64, 0);
        System.out.println(pointShoot.describeCamera());
        System.out.println(phone.describeCamera());
    }
}
